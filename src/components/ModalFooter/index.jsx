import PropTypes from 'prop-types';

const ModalFooter = ({ firstText="", secondaryText="", firstClick = ()=>{}, secondaryClick=()=>{} }) => {

    return (
        <div className="modal__footer">
            {firstText && <button onClick={firstClick} className='btn'>{firstText}</button>}
            {secondaryText && <button onClick={secondaryClick} className='btn btn-delete'>{secondaryText}</button>}
        </div>
    )
}

ModalFooter.propTypes = {
    firstText: PropTypes.string.isRequired,
    secondaryText: PropTypes.string,
    firstClick: PropTypes.func,
    secondaryClick: PropTypes.func,
};

export default ModalFooter
