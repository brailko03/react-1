import PropTypes from 'prop-types';

const Button = ({ type = "button", className = "btn", onClick = () => { }, children }) => {

    return (
        <button type="button" className="btn" onClick={onClick}>{children}</button>
    );


};

Button.propTypes = {
    type: PropTypes.oneOf(['button', 'submit', 'reset']),
    classNames: PropTypes.string,
    onClick: PropTypes.func,
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]).isRequired
}

export default Button;
