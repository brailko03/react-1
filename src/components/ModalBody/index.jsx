import PropTypes from 'prop-types';

const ModalBody = ({ children, onClick = () => { } }) => {

    return (
        <>
            <div>
                {children}
            </div>
        </>
    );


};


ModalBody.propTypes = {
    children: PropTypes.node,
    onClick: PropTypes.func,
}



export default ModalBody 
