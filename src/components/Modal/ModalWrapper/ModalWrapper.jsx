import React from 'react';
import PropTypes from 'prop-types';

const ModalWrapper = ({children, onClick = () => {}}) => {

    return (
        <>
            <div className='wrapper' onClick={onClick}>{children}</div>
        </>
    );
}


export default ModalWrapper;
