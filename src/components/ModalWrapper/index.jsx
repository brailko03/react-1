import PropTypes from 'prop-types';

const ModalWrapper = ({ children, onClick = () => { } }) => {

    return (
        <>
            <div className='wrapper' onClick={onClick}>{children}</div>
        </>

    );
};

ModalWrapper.propTypes = {

    children: PropTypes.node,
    onClick: PropTypes.func,

}

export default ModalWrapper;
