import PropTypes from "prop-types"

const ModalHeader = ({ children }) => {


    return (
        <h2> 
            {children}
        </h2>
    )
}

ModalHeader.propTypes = {
    children: PropTypes.node.isRequired,
}

export default ModalHeader;
